﻿namespace OtusHW2._3
{
    internal interface IDataProvider
    {
        void SaveAll(string data);
        string ReadAll();
    }
}