﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace OtusHW2._3
{
    internal class Repository : IRepository<Account>
    {
        private readonly IDataProvider _dataProvider;

        public Repository(IDataProvider dataProvider)
        {
            _dataProvider = dataProvider;
        }


        public IEnumerable<Account> GetAll()
        {
            var data = Read();
            if (data == null)
                return new List<Account>();
            return Read();
        }

        public Account GetOne(Func<Account, bool> predicate)
        {
            var items = Read();
            return items.FirstOrDefault(predicate);
        }

        public void Add(Account item)
        {
            var accounts = Read() ?? new List<Account>();
            accounts.Add(item);
            Save(accounts);
        }

        private void Save(IEnumerable<Account> accounts)
        {
            _dataProvider.SaveAll(JsonConvert.SerializeObject(accounts));
        }

        private List<Account> Read()
        {
            return JsonConvert.DeserializeObject<List<Account>>(_dataProvider.ReadAll());
        }
    }
}