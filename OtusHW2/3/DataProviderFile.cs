﻿using System.IO;

namespace OtusHW2._3
{
    internal class DataProviderFile : IDataProvider
    {
        private readonly string _pathRepository;

        public DataProviderFile(string path)
        {
            //     _pathRepository = path;
            var tmpDir = Path.Combine(Directory.GetCurrentDirectory(), "Temp");
            if (!Directory.Exists(tmpDir))
                Directory.CreateDirectory(tmpDir);
            _pathRepository = Path.Combine(tmpDir, path);

            if (File.Exists(_pathRepository))
                File.Delete(_pathRepository);
        }

        public void SaveAll(string data)
        {
            File.WriteAllText(_pathRepository, data);
        }

        public string ReadAll()
        {
            if (!File.Exists(_pathRepository))
                return string.Empty;
            return File.ReadAllText(_pathRepository);
        }
    }
}