﻿using System;

namespace OtusHW2._3
{
    public class AccountService : IAccountService
    {
        private readonly IRepository<Account> _accountRepository;

        public AccountService(IRepository<Account> accountRepository)
        {
            _accountRepository = accountRepository;
        }

        public void AddAccount(Account account)
        {
            Validate(account);
            _accountRepository.Add(account);
        }

        private void Validate(Account account)
        {
            if (account == null)
                throw new ArgumentNullException(nameof(account));

            if (string.IsNullOrWhiteSpace(account.FirstName))
                throw new ArgumentException("First Name cannot be empty.");

            if (string.IsNullOrWhiteSpace(account.LastName))
                throw new ArgumentException("Last Name cannot be empty.");


            if (account.BirthDate > DateTime.Now.AddYears(-18))
                throw new ArgumentException("The value BirthDate is not valid");
        }
    }
}