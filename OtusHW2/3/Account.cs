﻿using System;

namespace OtusHW2
{
    public class Account
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }

        public override string ToString()
        {
            return $"FirstName={FirstName},LastName={LastName}";
        }
    }
}