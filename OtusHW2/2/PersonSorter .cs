﻿using System.Collections.Generic;
using System.Linq;

namespace OtusHW2
{
    internal class PersonSorter : IAlgorithm<Person>
    {
        public IEnumerable<Person> Sort(IEnumerable<Person> items)
        {
            return items?.OrderBy(i => i.Age);
        }
    }
}