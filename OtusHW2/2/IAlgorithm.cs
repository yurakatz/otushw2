﻿using System.Collections.Generic;

namespace OtusHW2
{
    internal interface IAlgorithm<T>
    {
        IEnumerable<T> Sort(IEnumerable<T> items);
    }
}