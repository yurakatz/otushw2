﻿using System.IO;
using System.Xml;
using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;

namespace OtusHW2
{
    internal class OtusXmlSerializer : ISerializer
    {
        public void Serialize<T>(T item, string pathSting)
        {
            var serializer = new ConfigurationContainer().Create();
            var contents = serializer.Serialize(new XmlWriterSettings {Indent = true}, item);
            File.WriteAllText(pathSting, contents);
        }

        public T Deserialize<T>(Stream stream)
        {
            T deserialized;
            var serializer = new ConfigurationContainer().Create();
            var readerSettings = new XmlReaderSettings {IgnoreWhitespace = false};
            using (var reader = XmlReader.Create(stream, readerSettings))
            {
                deserialized = (T) serializer.Deserialize(reader);
            }

            return deserialized;
        }
    }
}