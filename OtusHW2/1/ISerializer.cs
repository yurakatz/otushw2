﻿using System.IO;

namespace OtusHW2
{
    public interface ISerializer
    {
        void Serialize<T>(T item, string pathSting);
        T Deserialize<T>(Stream stream);
    }
}