﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace OtusHW2
{
    internal class OtusStreamReader<T> : IEnumerable<T>, IDisposable
    {
        private readonly ISerializer _serializer;
        private readonly Stream _stream;

        public OtusStreamReader(Stream stream, ISerializer serializer)
        {
            _stream = stream;
            _serializer = serializer;
        }

        public IEnumerator<T> GetEnumerator()
        {
            var data = _serializer.Deserialize<T[]>(_stream);
            foreach (var item in data)
                yield return item;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #region IDisposable implementation

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool _mDisposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_mDisposed)
            {
                if (disposing) _stream?.Dispose();
                _mDisposed = true;
            }
        }

        ~OtusStreamReader()
        {
            Dispose(false);
        }

        #endregion
    }
}