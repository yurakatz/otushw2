﻿using System.IO;

namespace OtusHW2
{
    internal static class Helper
    {
        public static string PathXml { get; private set; }

        public static void Init()
        {
            var tmpDir = Path.Combine(Directory.GetCurrentDirectory(), "Temp");
            if (!Directory.Exists(tmpDir))
                Directory.CreateDirectory(tmpDir);
            PathXml = Path.Combine(tmpDir, "test.xml");

            InitXmlFile();
        }

        private static void InitXmlFile()
        {
            Person[] outList =
            {
                new Person {FirstName = "FirstName1", LastName = "LastName1", Age = 56},
                new Person {FirstName = "FirstName2", LastName = "LastName2", Age = 30},
                new Person {FirstName = "FirstName3", LastName = "LastName3", Age = 15},
                new Person {FirstName = "FirstName4", LastName = "LastName4", Age = 3}
            };


            var temp = new OtusXmlSerializer();
            temp.Serialize(outList, PathXml);
        }
    }
}