﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using OtusHW2._3;

namespace OtusHW2
{
    internal class Program
    {
        private static void Main()
        {
            Helper.Init();

            var personList = new List<Person>();
            var personReader = new OtusStreamReader<Person>(
                new FileStream(Helper.PathXml, FileMode.Open, FileAccess.Read),
                new OtusXmlSerializer());


            foreach (var variable in personReader)
            {
                personList.Add(variable);
                Debug.WriteLine(variable.ToString());
            }

            var sorted = new PersonSorter().Sort(personList);


            foreach (var variable in sorted) Debug.WriteLine(variable.ToString());


            var rep = new Repository(new DataProviderFile("Repository.json"));

            var items = rep.GetAll();

            foreach (var variable in items) Debug.WriteLine(variable.ToString());
            rep.Add(new Account {FirstName = "fn", LastName = "ln", BirthDate = DateTime.Now.AddYears(-20)});
            items = rep.GetAll();
            foreach (var variable in items) Debug.WriteLine(variable.ToString());
        }
    }
}