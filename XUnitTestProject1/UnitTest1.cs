﻿using System;
using Moq;
using OtusHW2;
using OtusHW2._3;
using Xunit;

namespace XUnitTestProject1
{
    public class UnitTest1
    {
        [Fact]
        public void AccountServiceTest_Add()
        {
            var newUser = new Account {LastName = "Test", FirstName = "Test", BirthDate = DateTime.Now.AddYears(-20)};
            var mock = new Mock<IRepository<Account>>();
            var accountService = new AccountService(mock.Object);
            mock.Setup(x => x.GetAll());
            mock.Setup(x => x.GetOne(It.IsAny<Func<Account, bool>>()));
            mock.Setup(x => x.Add(It.IsAny<Account>()));

            accountService.AddAccount(newUser);

            mock.Verify(x => x.Add(It.IsAny<Account>()));
            mock.Verify(x => x.Add(It.IsAny<Account>()), Times.Once());
        }

        [Fact]
        public void AccountServiceTest_Validation_Age()
        {
            var newUser = new Account
                {LastName = "Test", FirstName = "Test", BirthDate = DateTime.Now.AddYears(-10)};
            var mock = new Mock<IRepository<Account>>();
            var accountService = new AccountService(mock.Object);
            mock.Setup(x => x.GetAll());
            mock.Setup(x => x.GetOne(It.IsAny<Func<Account, bool>>()));
            mock.Setup(x => x.Add(It.IsAny<Account>()));


            Assert.Throws<ArgumentException>(() => accountService.AddAccount(newUser));
        }

        [Fact]
        public void AccountServiceTest_Validation_FirstName()
        {
            var newUser = new Account {LastName = "Test", BirthDate = DateTime.Now.AddYears(-20)};
            var mock = new Mock<IRepository<Account>>();
            var accountService = new AccountService(mock.Object);
            mock.Setup(x => x.GetAll());
            mock.Setup(x => x.GetOne(It.IsAny<Func<Account, bool>>()));
            mock.Setup(x => x.Add(It.IsAny<Account>()));

            Assert.Throws<ArgumentException>(() => accountService.AddAccount(newUser));
        }

        [Fact]
        public void AccountServiceTest_Validation_LastName()
        {
            var newUser = new Account {FirstName = "Test", BirthDate = DateTime.Now.AddYears(-20)};
            var mock = new Mock<IRepository<Account>>();
            var accountService = new AccountService(mock.Object);
            mock.Setup(x => x.GetAll());
            mock.Setup(x => x.GetOne(It.IsAny<Func<Account, bool>>()));
            mock.Setup(x => x.Add(It.IsAny<Account>()));

            Assert.Throws<ArgumentException>(() => accountService.AddAccount(newUser));
        }

        [Fact]
        public void AccountServiceTest_Validation_null()
        {
            var mock = new Mock<IRepository<Account>>();
            var accountService = new AccountService(mock.Object);
            mock.Setup(x => x.GetAll());
            mock.Setup(x => x.GetOne(It.IsAny<Func<Account, bool>>()));
            mock.Setup(x => x.Add(It.IsAny<Account>()));

            Assert.Throws<ArgumentNullException>(() => accountService.AddAccount(null));
        }
    }
}